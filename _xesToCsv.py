import xml.etree.cElementTree as tree
import gzip
from os.path import abspath, join, isfile, dirname
from os import listdir
import time
import pandas as pd

#parameters
to_excel = 0 # 1 - save results to .xslx file
to_csv = 1 # 1 - save results to .csv file
log_to_file = 1 # 1 - use logs
              

"""get specific type of files"""
def getFileListForType(fileType):
    path = abspath(dirname(__file__))
    return [f for f in listdir(path) if isfile(join(path,f)) and f.endswith(fileType) ]

"""log to logs.txt"""
def toLog(message,enable=log_to_file,before=''):
    if enable == 1:
        with open('logs.txt','a+') as log:
            log.write(before + time.strftime('%Y:%m:%d %H:%M:%S') + ' || '+ message + '\n')

"""open gzfile and save it"""
def openAndSavegzFiles():
    gzFileList = getFileListForType(".gz")
    toLog('Found '+str(len(gzFileList))+' .gz files and try to unpack')
    for gzFile in gzFileList: 
        with gzip.open(gzFile, "rb") as sourceFile:
            
            start_time = time.time()
            data = sourceFile.read()
            print("Reading data: ", str(time.time() - start_time))
            
            with open(gzFile[0:-4], 'wb') as targetFile: #assume that end of the file is "_.gz"
                targetFile.write(data)
        toLog('.gz files unpacked.')
                
def insertDataBase(table,values):
    pass

def saveInsertToDataBase(targetFile):
    pass

def createTableDataBase(table,fields):
    pass

def connectToDatabase(query):
    pass

####MAIN####
toLog('New run started',before='\n\n')
#unpack .gz files in current folder
openAndSavegzFiles()

#open and parse xml
xesFiles = getFileListForType('.xes')
for xesFile in xesFiles:
    toLog(xesFile+ ' - start')
    file_time = time.time()
    with open(xesFile, "r+") as currentFile:
        
        #parse xml
        start_time = time.time()
        xmlTree = tree.parse(currentFile)
        root = xmlTree.getroot()
        
        endTime = xesFile+ " - parsing time: "+ str(time.time() - start_time)
        print(endTime)
        toLog(endTime)
        
        #traverse xml
        start_time = time.time()
        caselog = pd.DataFrame() #store cases
        eventlog = pd.DataFrame() #store events
        
        #get file names
        for field in root:
            if field.attrib.get('key') == "concept:name":
                inFileName = field.attrib.get('value')
        
        uniqueKey = 0
        eventCount = 0
        #get cases
        for case in root: #go to case level
            if case.tag == 'trace':
                uniqueKey += 1
                caselog.at[uniqueKey,'ID'] = uniqueKey
                caselog.at[uniqueKey,'fileName'] = inFileName
                caselog.at[uniqueKey,'fullFileName'] = xesFile
                
                for field in case:
                    if field.attrib:
                        caselog.at[uniqueKey,field.attrib['key'].replace(':',"_")] = field.attrib['value']
                
                # get events
                for event in case: #go to event level
                    if event.tag == 'event':
                        eventCount += 1
                        eventlog.at[eventCount,'ID'] = uniqueKey
                        for field in event:
                            if field.attrib:
                                eventlog.at[eventCount,field.attrib['key'].replace(':',"_")] = field.attrib['value']
        
        endTime = xesFile+ " - traverse time: "+ str(time.time() - start_time)
        print(endTime)
        toLog(endTime)
        
        #save to files
        start_time = time.time()
        fileName = inFileName.replace(' ','')+"{}.{}"
        #save to csv
        if to_csv == 1:
            eventlog.to_csv(path_or_buf= fileName.format('_eventlog','csv'),sep=';',index=False)
            toLog(xesFile +'- eventlog saved to '+fileName.format('_eventlog','csv'))
            caselog.to_csv(path_or_buf= fileName.format('_caselog','csv'),sep=';',index=False)
            toLog(xesFile +'- caselog saved to '+fileName.format('_caselog','csv'))
        
        #save to excel
        if to_excel == 1:
            caselog.to_excel(excel_writer= fileName.format('','xlsx'),sheet_name='caselog',index=False)
            with pd.ExcelWriter(path=fileName.format('','xlsx'), mode='a') as writer:
                eventlog.to_excel(excel_writer=writer,sheet_name='eventlog',index=False)
            toLog(xesFile +'- caselog&eventlog saved to '+fileName.format('','xlsx'))

        endTime = xesFile+ " - saving time: "+ str(time.time() - start_time)
        print(endTime)
        toLog(endTime)
        
        #delete from memory
        del eventlog
        del caselog
        del xmlTree
        
        endTime = xesFile+ " - total time: "+ str(time.time() - file_time)
        print(endTime)
        toLog(endTime)
        

