/*
/version: 1.5
/Desc: Create case table and eventlog for files for BPI Challenge. It will take ID and DD as first priority, TP as second, PTC as third and RFP as fourth.
/Input: 
/	Tables in databas - you can get them as result of python script to extract data from .xes file (save to .csv and use wizard to load .csv):
/		DomesticDeclaration_caselog
/		[DomesticDeclarations_eventlog]
/		[InternationalDeclarations_caselog]
/		[InternationalDeclarations_eventlog]
/		[PrepaidTravelCosts_caselog]
/		[PrepaidTravelCosts_eventlog]
/		[RequestForPayment_caselog]
/		[RequestForPayment_eventlog]
/		[TravelPermits_caselog]
/		[TravelPermits_eventlog]
/Output:
/	case and event table with schema provided in input parameters
/Changelog:
/	1.0 - first version
/	1.1 - bug fixing
/	1.2 - bug fixing
/	1.3 - bug fixing
/	1.4 - bug fixing
/	1.5 - bug fixing
*/
CREATE OR ALTER         PROCEDURE [dbo].[createCasesAndEventsMain] 
(
	 @caseTable			NVARCHAR(255) = 'cases'		--output case table name
	,@eventTable		NVARCHAR(255) = 'eventlog'	--output event table name
	,@schema			NVARCHAR(255) = 'dm1'		--output schema name - user needs permission to create schema if schema doesn't exists!
	,@add_TP			BIT			  = 1			-- if TP cases and events should be treated as anchor point
	,@add_PTC			BIT		      = 1			-- if PTC cases and events should be treated as anchor point
	,@add_RFP			BIT			  = 1			-- if RFP cases and events should be treated as anchor point
)
AS BEGIN
	DECLARE
		 @sql			NVARCHAR(MAX)
		,@msg			NVARCHAR(max) = ''
		,@commonSql		NVARCHAR(MAX)
	;

	SET @caseTable = @schema+'.'+@caseTable;
	SET @eventTable = @schema+'.'+@eventTable;

-- =============================================
-- CREATE SCHEMA
-- =============================================
	IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = @schema) BEGIN
		SET @sql = 'CREATE SCHEMA '+ @schema;
		EXEC sp_executesql @sql;
	END

-- =============================================
-- (RE)CREATE TABLES
-- =============================================
	--create empty cases table
	IF(OBJECT_ID(@caseTable, 'U') IS NOT NULL) BEGIN		
		SET @sql = 'DROP TABLE '+@caseTable;
		EXEC sp_executesql @sql;
		SET @msg = 're';
	END;

	SET @sql = 'CREATE TABLE '+@caseTable+' (
		  caseID NVARCHAR(255) NOT NULL
		, caseType NVARCHAR(255) NOT NULL
		, sourceID NVARCHAR(255) NOT NULL
		, Amount float
		, RequestedAmountDeclaration float
		, PermitTaskNumber NVARCHAR(255)
		, PermitBudgetNumber NVARCHAR(255)
		, OriginalAmount float
		, PermitProjectNumber NVARCHAR(255)
		, PermiRequestedBudget float
		, PermtitID NVARCHAR(255)
		, BudgetNumber NVARCHAR(255)
		, PermitActivityNumber NVARCHAR(255)
		, AdjustedAmount float
		, Task NVARCHAR(255)
		, Project NVARCHAR(255)
		, Permit_OrganizationalEntity NVARCHAR(255)
		, Permit_Cost_Type NVARCHAR(255)
		, Cost_Type NVARCHAR(255)
		, OrganizationalEntity NVARCHAR(255)
		, RequestedAmountTravelPermit NVARCHAR(255)
		, TotalDeclared float
		, Overspent bit
		, RequestedBudget float
		, OverspentAmount float
		, RequestedAmountRequestForPayment float
		, RequestedAmountPrepaidTravelCosts float
		, Activity NVARCHAR(255)
		)';
	EXEC sp_executesql @sql;
	SET @msg = @caseTable+' '+@msg+'created'
	RAISERROR(@msg,0,0) WITH NOWAIT;
	SET @msg = '';

    IF(TYPE_ID('dbo.eventlog') IS NULL) BEGIN
        SET @sql = 'CREATE TYPE [dbo].[eventlog] AS TABLE(
        [caseID] [nvarchar](255) NOT NULL,
        [resourceName] [nvarchar](255) NULL,
        [eventName] [nvarchar](255) NULL,
        [timestamp] [datetime] NULL,
        [sourceTimestamp] [nvarchar](255) NULL,
        [roleName] [nvarchar](255) NULL,
        [sourceEventName] [nvarchar](255) NULL,
        [documentID] [nvarchar](255) NULL,
        [sourceID] [nvarchar](255) NULL,
        [sourceTable] [nvarchar](255) NULL
            )';
        EXEC sp_executesql @sql;
	    SET @msg = 'eventlog type created'
	    RAISERROR(@msg,0,0) WITH NOWAIT;
    END;

    	--create empty eventlog
	IF(OBJECT_ID(@eventTable, 'U') IS NOT NULL) BEGIN		
		SET @sql = 'DROP TABLE '+@eventTable;
		EXEC sp_executesql @sql;
		SET @msg = 're';
	END;

    SET @sql = 'DECLARE @eventTable dbo.eventlog
                SELECT *
                INTO '+@eventTable+'
                FROM @eventTable
                WHERE 1=2'
            ;

	EXEC sp_executesql @sql;
	SET @msg = @eventTable+' '+@msg+'created'
	RAISERROR(@msg,0,0) WITH NOWAIT;
	SET @msg = '';

-- =============================================
-- ADD CASES - first level - ID,DD as anchor
-- =============================================
	--Domestic:
	SET @sql = ' INSERT INTO '+@caseTable+
			   ' SELECT 
					  id1
					, ''DomesticDeclaration''
					, id1
					, Amount
					, NULL
					, NULL
					, NULL
					, NULL
					, NULL
					, NULL
					, NULL
					, [BudgetNumber]
					, NULL
					, NULL
					, NULL
					, NULL
					, NULL
					, NULL
					, NULL
					, NULL
					, NULL
					, NULL
					, NULL
					, NULL
					, NULL
					, NULL
					, NULL
					, NULL
			   FROM DomesticDeclarations_caselog
			   ';
	EXEC sp_executesql @sql;
	SET @msg = 'CASES: Domestic declaration added to '+@caseTable;
	RAISERROR(@msg,0,0) WITH NOWAIT;
	SET @msg = '';

	--International:
	SET @sql = ' INSERT INTO '+@caseTable+
			   ' SELECT 
					  id.id1
					, ''InternationalDeclaration''
					, id.id1
					, id.Amount
					, id.RequestedAmount
					, COALESCE(id.[Permit_TaskNumber],tp.TaskNumber,tp.Task_0,ptc.[Permit_TaskNumber]) Permit_TaskNumber
					, COALESCE(id.[Permit_BudgetNumber],tp.BudgetNumber,ptc.[Permit_BudgetNumber]) Permit_BudgetNumber
					, id.OriginalAmount
					, COALESCE(id.[Permit_ProjectNumber],tp.ProjectNumber,tp.Project_0,ptc.[Permit_ProjectNumber]) Permit_ProjectNumber
					, COALESCE(id.[Permit_RequestedBudget],ptc.Permit_RequestedBudget) Permit_RequestedBudget
					, id.[Permit_ID]
					, COALESCE(id.[BudgetNumber],tp.BudgetNumber) BudgetNumber
					, COALESCE(id.[Permit_ActivityNumber],tp.ActivityNumber)
					, id.[AdjustedAmount]
					--new:
					, ptc.Task
					, ptc.Project
					, COALESCE(tp.OrganizationalEntity,tp.OrganizationalEntity_0,ptc.Permit_OrganizationalEntity)
					, tp.Cost_Type_0 AS Permit_Cost_Type
					, ptc.Cost_Type AS Cost_Type
					, ptc.OrganizationalEntity
					--added by OL:
					, tp.RequestedAmount RequestedAmountTP
					, tp.TotalDeclared
					, tp.Overspent
					, tp.RequestedBudget
					, tp.OverspentAmount
					, ptc.RequestedAmountFRP
					, ptc.RequestedAmountPTC
					, ptc.Activity

			   FROM InternationalDeclarations_caselog id
			   LEFT JOIN 
				   (SELECT
						 id.id1
						,MAX(tp.TaskNumber) TaskNumber
						,MAX(tp.Task_0) Task_0
						,MAX(tp.ProjectNumber) ProjectNumber
						,MAX(tp.Project_0) Project_0
						,MAX(tp.OrganizationalEntity_0) OrganizationalEntity_0
						,MAX(tp.OrganizationalEntity) OrganizationalEntity
						,MAX(tp.Cost_Type_0) Cost_Type_0
						,MAX(tp.BudgetNumber) BudgetNumber
						--added by OL:
						,MAX(tp.ActivityNumber) ActivityNumber
						,SUM(tp.TotalDeclared) TotalDeclared
						,SUM(tp.RequestedAmount_0) RequestedAmount
						,MAX(CAST(tp.Overspent AS INT)) Overspent
						,SUM(tp.RequestedBudget) RequestedBudget
						,SUM(tp.OverspentAmount) OverspentAmount

				   FROM TravelPermits_caselog tp
				   JOIN InternationalDeclarations_caselog id
			   			ON tp.concept_name = id.[Permit_ID]
				   GROUP BY id.id1
				   ) tp
				ON tp.id1 = id.id1
			   LEFT JOIN 
					(SELECT
						 id.id1
						,COALESCE(MAX(ptc.Task),MAX(rfp.Task)) Task
						,COALESCE(MAX(ptc.Project),MAX(rfp.Project)) Project
						,MAX(ptc.Permit_TaskNumber) Permit_TaskNumber
						,MAX(ptc.Permit_RequestedBudget) Permit_RequestedBudget
						,MAX(ptc.Permit_ProjectNumber) Permit_ProjectNumber
						,MAX(ptc.Permit_OrganizationalEntity) Permit_OrganizationalEntity
						,MAX(ptc.Permit_BudgetNumber) Permit_BudgetNumber
						,COALESCE(MAX(ptc.OrganizationalEntity),MAX(rfp.OrganizationalEntity)) OrganizationalEntity
						,COALESCE(MAX(ptc.Cost_Type),MAX(rfp.Cost_Type)) Cost_Type
						--added by OL:
						,SUM(ptc.RequestedAmount) RequestedAmountPTC
						, SUM(rfp.RequestedAmount) RequestedAmountFRP
						,COALESCE(MAX(ptc.Activity),MAX(rfp.Activity)) Activity

					FROM PrepaidTravelCosts_caselog ptc
					LEFT JOIN 
						(SELECT 
							 ptc.concept_name
							,MAX(rfp.Task) Task
							,MAX(rfp.Project) Project
							,MAX(rfp.OrganizationalEntity) OrganizationalEntity
							,MAX(rfp.Cost_Type) Cost_Type
							--added by OL:
							,SUM(rfp.RequestedAmount) RequestedAmount
							,MAX(rfp.Activity) Activity

						FROM RequestForPayment_caselog rfp
						JOIN PrepaidTravelCosts_caselog ptc
							ON rfp.RfpNumber = ptc.RfpNumber
						WHERE rfp.RfpNumber != ''UNKNOWN''
						GROUP BY ptc.concept_name
						) rfp
						ON ptc.concept_name = rfp.concept_name
				   JOIN InternationalDeclarations_caselog id
			   			ON ptc.Permit_id = id.[Permit_ID]
				   GROUP BY id.id1
				   ) ptc
			    ON ptc.id1 = id.id1
				';
	EXEC sp_executesql @sql;
	SET @msg = 'CASES: International declaration added to '+@caseTable;
	RAISERROR(@msg,0,0) WITH NOWAIT;
	SET @msg = '';

-- =============================================
-- ADD EVENTS - common part
-- =============================================
--common part:
	SET @commonSql = ' INSERT INTO ##eventlog
			        SELECT 
					  c.caseID
					, e.org_resource
					, CASE WHEN CHARINDEX('' by '',e.concept_name) != 0
						  THEN LEFT(e.concept_name,CHARINDEX('' by '',e.concept_name))
						  ELSE e.concept_name
					  END
					--, CAST(LEFT(e.time_timestamp,10) AS DATETIME)+CAST(SUBSTRING(e.time_timestamp,12,8) AS DATETIME)-CAST(RIGHT(e.time_timestamp,5) AS DATETIME)
					, e.time_timestamp
                    , e.time_timestamp
					, e.org_role
					, e.concept_name
					';

-- =============================================
-- ADD EVENTS - first level - ID,DD as anchor
-- =============================================
	--fill eventlog:
        -- [caseID] [nvarchar](255) NOT NULL,
        -- [resourceName] [nvarchar](255) NULL,
        -- [eventName] [nvarchar](255) NULL,
        -- [timestamp] [datetime] NULL,
        -- [sourceTimestamp] [nvarchar](255) NULL,
        -- [roleName] [nvarchar](255) NULL,
        -- [sourceEventName] [nvarchar](255) NULL,
        -- [documentID] [nvarchar](255) NULL,
        -- [sourceID] [nvarchar](255) NULL,
        -- [sourceTable] [nvarchar](255) NULL

	IF OBJECT_ID('tempdb..##eventlog') IS NOT NULL BEGIN
			DROP TABLE ##eventlog;
			SET @msg = 'EVENTS: ##eventlog dropped';
			RAISERROR(@msg,0,0) WITH NOWAIT; 
	END;

    SET @sql = 'DECLARE @eventTable dbo.eventlog
                SELECT *
                INTO ##eventlog
                FROM @eventTable
                WHERE 1=2'
            ;
	EXEC sp_executesql @sql;
	SET @msg = 'EVENTS: Temp table created';
	RAISERROR(@msg,0,0) WITH NOWAIT;
					
	--Events Domestic:
	SET @sql = @commonSql+', c2.concept_name, e.id1, ''DomesticDeclarations_eventlog''
			    FROM '+@caseTable+' c
			    JOIN DomesticDeclarations_caselog c2
				     ON c2.id1 = c.sourceID
				JOIN DomesticDeclarations_eventlog e
					ON c2.ID = e.ID
				WHERE e.id1 NOT IN (SELECT sourceID FROM ##eventlog GROUP BY sourceID)'
				;
	EXEC sp_executesql @sql;
	SET @msg = 'EVENTS: Domestic declaration added to '+@eventTable+'. Anchor: ID.';
	
	RAISERROR(@msg,0,0) WITH NOWAIT;

	--Events International:
	SET @sql = @commonSql+', c2.concept_name, e.id1,''InternationalDeclarations_eventlog''
			    FROM '+@caseTable+' c
			    JOIN InternationalDeclarations_caselog c2
				     ON c2.id1 = c.sourceID
				JOIN InternationalDeclarations_eventlog e
					ON c2.ID = e.ID
				WHERE e.id1 NOT IN (SELECT sourceID FROM ##eventlog GROUP BY sourceID)'
				;
	EXEC sp_executesql @sql;
	
	SET @msg = 'EVENTS: International declaration added to '+@eventTable+'. Anchor: ID.';
	RAISERROR(@msg,0,0) WITH NOWAIT;

	--Events TravelPermits
	SET @sql = @commonSql+', tp.concept_name, e.id1,''TravelPermits_eventlog''
			    FROM '+@caseTable+' c
			    JOIN InternationalDeclarations_caselog c2
				     ON c2.id1 = c.sourceID
				JOIN TravelPermits_caselog tp
				     ON tp.concept_name = c2.[Permit_ID]
				JOIN TravelPermits_eventlog e
					ON tp.ID = e.ID
				WHERE e.id1 NOT IN (SELECT sourceID FROM ##eventlog GROUP BY sourceID)
				';
	EXEC sp_executesql @sql;
	
	SET @msg = 'EVENTS: Travel permits added to '+@eventTable+'. Anchor: ID.';
	RAISERROR(@msg,0,0) WITH NOWAIT;

	--Events PrepareTravelCosts:
	SET @sql = @commonSql+', ptc.concept_name, e.id1,''PrepaidTravelCosts_eventlog''
			    FROM '+@caseTable+' c
			    JOIN InternationalDeclarations_caselog c2
				     ON c2.id1 = c.sourceID
				JOIN PrepaidTravelCosts_caselog ptc
				     ON ptc.[Permit_ID] = c2.[Permit_ID]
				JOIN PrepaidTravelCosts_eventlog e
					ON ptc.ID = e.ID
				WHERE e.id1 NOT IN (SELECT sourceID FROM ##eventlog GROUP BY sourceID)
				';
	EXEC sp_executesql @sql;
	
	SET @msg = 'EVENTS: Prepaid travel costs added to '+@eventTable+'. Anchor: ID.';
	RAISERROR(@msg,0,0) WITH NOWAIT;

	--Events PrepareTravelCosts:
	SET @sql = @commonSql+', rfp.concept_name, e.id1,''RequestForPayment_eventlog''
			    FROM '+@caseTable+' c
			    JOIN InternationalDeclarations_caselog c2
				     ON c2.id1 = c.sourceID
				JOIN PrepaidTravelCosts_caselog ptc
				     ON ptc.[Permit_ID] = c2.[Permit_ID]
				JOIN (SELECT * FROM RequestForPayment_caselog WHERE RfpNumber != ''UNKNOWN'') rfp
					ON rfp.RfpNumber = ptc.RfpNumber
				JOIN RequestForPayment_eventlog e
					ON rfp.ID = e.ID
				WHERE e.id1 NOT IN (SELECT sourceID FROM ##eventlog GROUP BY sourceID)
				';
	EXEC sp_executesql @sql;
	
	SET @msg = 'EVENTS: Request for Request for Payment added to '+@eventTable+'. Anchor: ID.';
	RAISERROR(@msg,0,0) WITH NOWAIT;

    --persist events
    SET @sql = 'INSERT INTO '+@eventTable+'
                SELECT * FROM ##eventlog'
                ;
	EXEC sp_executesql @sql;
	
	SET @msg = 'EVENTS: Persists';
	RAISERROR(@msg,0,0) WITH NOWAIT;    


	IF @add_TP = 1 BEGIN
	-- =============================================
	-- ADD CASES - first level - TP as anchor
	-- =============================================	

	SET @sql = ' INSERT INTO '+@caseTable+
			   ' SELECT 
					  tp.concept_name
					, ''Travel Permits''
					, tp.concept_name
					, NULL
					, NULL
					, COALESCE(tp.TaskNumber,tp.Task_0,ptc.[Permit_TaskNumber]) Permit_TaskNumber
					, COALESCE(tp.BudgetNumber,ptc.[Permit_BudgetNumber]) Permit_BudgetNumber
					, NULL
					, COALESCE(tp.ProjectNumber,tp.Project_0,ptc.[Permit_ProjectNumber]) Permit_ProjectNumber
					, ptc.Permit_RequestedBudget
					, NULL
					, tp.BudgetNumber 
					, tp.ActivityNumber
					, NULL
					--new:
					, ptc.Task
					, ptc.Project
					, COALESCE(tp.OrganizationalEntity,tp.OrganizationalEntity_0,ptc.Permit_OrganizationalEntity)
					, tp.Cost_Type_0 AS Permit_Cost_Type
					, ptc.Cost_Type AS Cost_Type
					, ptc.OrganizationalEntity
					--added by OL:
					, tp.RequestedAmount_0 RequestedAmountTP
					, tp.TotalDeclared
					, CAST(tp.Overspent AS INT) Overspent
					, tp.RequestedBudget
					, tp.OverspentAmount
					, ptc.RequestedAmountFRP
					, ptc.RequestedAmountPTC
					, ptc.Activity

			   FROM TravelPermits_caselog tp
			   LEFT JOIN 
					(SELECT
						 tp.concept_name
						,COALESCE(MAX(ptc.Task),MAX(rfp.Task)) Task
						,COALESCE(MAX(ptc.Project),MAX(rfp.Project)) Project
						,MAX(ptc.Permit_TaskNumber) Permit_TaskNumber
						,MAX(ptc.Permit_RequestedBudget) Permit_RequestedBudget
						,MAX(ptc.Permit_ProjectNumber) Permit_ProjectNumber
						,MAX(ptc.Permit_OrganizationalEntity) Permit_OrganizationalEntity
						,MAX(ptc.Permit_BudgetNumber) Permit_BudgetNumber
						,COALESCE(MAX(ptc.OrganizationalEntity),MAX(rfp.OrganizationalEntity)) OrganizationalEntity
						,COALESCE(MAX(ptc.Cost_Type),MAX(rfp.Cost_Type)) Cost_Type
						--added by OL:
						,SUM(ptc.RequestedAmount) RequestedAmountPTC
						, SUM(rfp.RequestedAmount) RequestedAmountFRP
						,COALESCE(MAX(ptc.Activity),MAX(rfp.Activity)) Activity

					FROM PrepaidTravelCosts_caselog ptc
					LEFT JOIN 
						(SELECT 
							 ptc.concept_name
							,MAX(rfp.Task) Task
							,MAX(rfp.Project) Project
							,MAX(rfp.OrganizationalEntity) OrganizationalEntity
							,MAX(rfp.Cost_Type) Cost_Type
							--added by OL:
							,SUM(rfp.RequestedAmount) RequestedAmount
							,MAX(rfp.Activity) Activity

						FROM RequestForPayment_caselog rfp
						JOIN PrepaidTravelCosts_caselog ptc
							ON rfp.RfpNumber = ptc.RfpNumber
						WHERE rfp.RfpNumber != ''UNKNOWN''
						GROUP BY ptc.concept_name
						) rfp
						ON ptc.concept_name = rfp.concept_name
				   JOIN TravelPermits_caselog tp
			   			ON ptc.[Permit_ID] = tp.concept_name
				   GROUP BY tp.concept_name
				   ) ptc
			    ON ptc.concept_name = tp.concept_name
				WHERE tp.concept_name NOT IN (SELECT DISTINCT documentID FROM '+@eventTable+')'
			   ;
		EXEC sp_executesql @sql;
		
		SET @msg = 'CASES: Travel Permits added to '+@caseTable;
		RAISERROR(@msg,0,0) WITH NOWAIT;
		SET @msg = '';

	-- =============================================
	-- ADD EVENTS - second level - TP as anchor
	-- =============================================

		IF OBJECT_ID('tempdb..##eventlog') IS NOT NULL BEGIN
			DROP TABLE ##eventlog;
			SET @msg = 'EVENTS: ##eventlog dropped';
			RAISERROR(@msg,0,0) WITH NOWAIT; 
		END;

		SET @sql = 'DECLARE @eventTable dbo.eventlog
					SELECT *
					INTO ##eventlog
					FROM @eventTable
					WHERE 1=2'
				;
		EXEC sp_executesql @sql;
		SET @msg = 'EVENTS: Temp table created';
		RAISERROR(@msg,0,0) WITH NOWAIT;

		--Events TravelPermits
		SET @sql = @commonSql+', c2.concept_name, e.id1,''TravelPermits_eventlog''
					FROM '+@caseTable+' c
					JOIN TravelPermits_caselog c2
						ON c2.concept_name = c.sourceID
					JOIN TravelPermits_eventlog e
						ON c2.ID = e.ID
					WHERE e.id1 NOT IN (SELECT sourceID FROM ##eventlog GROUP BY sourceID
										UNION ALL
										SELECT sourceID FROM '+@eventTable+' GROUP BY sourceID)
					';
		EXEC sp_executesql @sql;
		
		SET @msg = 'EVENTS: Travel permits added to '+@eventTable+'. Anchor: TP.';
		RAISERROR(@msg,0,0) WITH NOWAIT;

		--Events PrepareTravelCosts:
		SET @sql = @commonSql+', ptc.concept_name, e.id1,''PrepaidTravelCosts_eventlog''
					FROM '+@caseTable+' c
					JOIN TravelPermits_caselog c2
						ON c2.concept_name = c.sourceID
					JOIN PrepaidTravelCosts_caselog ptc
						ON ptc.[Permit_ID] = c2.concept_name
					JOIN PrepaidTravelCosts_eventlog e
						ON ptc.ID = e.ID
					WHERE e.id1 NOT IN (SELECT sourceID FROM ##eventlog GROUP BY sourceID
										UNION ALL
										SELECT sourceID FROM '+@eventTable+' GROUP BY sourceID)
					';
		EXEC sp_executesql @sql;
		
		SET @msg = 'EVENTS: Prepaid travel costs added to '+@eventTable+'. Anchor: TP.';
		RAISERROR(@msg,0,0) WITH NOWAIT;

		--Events PrepareTravelCosts:
		SET @sql = @commonSql+', rfp.concept_name, e.id1,''RequestForPayment_eventlog''
					FROM '+@caseTable+' c
					JOIN TravelPermits_caselog c2
						ON c2.concept_name = c.sourceID
					JOIN PrepaidTravelCosts_caselog ptc
						ON ptc.[Permit_ID] = c2.concept_name
					JOIN (SELECT * FROM RequestForPayment_caselog WHERE RfpNumber != ''UNKNOWN'') rfp
						ON rfp.RfpNumber = ptc.RfpNumber
					JOIN RequestForPayment_eventlog e
						ON rfp.ID = e.ID
					WHERE e.id1 NOT IN (SELECT sourceID FROM ##eventlog GROUP BY sourceID
										UNION ALL
										SELECT sourceID FROM '+@eventTable+' GROUP BY sourceID)
					';
		EXEC sp_executesql @sql;
		
		SET @msg = 'EVENTS: Request for Request for Payment added to '+@eventTable+'. Anchor: TP.';
		RAISERROR(@msg,0,0) WITH NOWAIT;

		--persist events
		SET @sql = 'INSERT INTO '+@eventTable+'
					SELECT * FROM ##eventlog'
					;
		EXEC sp_executesql @sql;
		
		SET @msg = 'EVENTS: Persists';
		RAISERROR(@msg,0,0) WITH NOWAIT; 

	END

	IF @add_PTC  = 1 BEGIN
	-- =============================================
	-- ADD CASES - first level - PTC as anchor
	-- =============================================

		SET @sql = ' INSERT INTO '+@caseTable+					
				   ' SELECT 
					  ptc.concept_name
					, ''Prepaid Travel Costs''
					, ptc.concept_name
					, NULL
					, NULL
					, ptc.[Permit_TaskNumber]
					, ptc.[Permit_BudgetNumber]
					, NULL
					, ptc.[Permit_ProjectNumber]
					, ptc.Permit_RequestedBudget
					, NULL
					, NULL
					, NULL
					, NULL
					--new:
					, COALESCE(ptc.Task,rfp.Task)
					, COALESCE(ptc.Project,rfp.Project)
					, ptc.Permit_OrganizationalEntity
					, NULL
					, COALESCE(ptc.Cost_Type,rfp.Cost_Type) AS Cost_Type
					, COALESCE(ptc.OrganizationalEntity,rfp.OrganizationalEntity)
					--added by OL:
					, NULL
					, NULL
					, NULL
					, NULL
					, NULL
					, rfp.RequestedAmountRFP
					, ptc.RequestedAmount
					, COALESCE(ptc.Activity,rfp.Activity)

				FROM PrepaidTravelCosts_caselog ptc
				LEFT JOIN 
						(SELECT 
							 ptc.concept_name
							,MAX(rfp.Task) Task
							,MAX(rfp.Project) Project
							,MAX(rfp.OrganizationalEntity) OrganizationalEntity
							,MAX(rfp.Cost_Type) Cost_Type
							--added by OL:
							,SUM(rfp.RequestedAmount) RequestedAmountRFP
							,MAX(rfp.Activity) Activity

						FROM RequestForPayment_caselog rfp
						JOIN PrepaidTravelCosts_caselog ptc
							ON rfp.RfpNumber = ptc.RfpNumber
						WHERE rfp.RfpNumber != ''UNKNOWN''
						GROUP BY ptc.concept_name
						) rfp
						ON ptc.concept_name = rfp.concept_name
				WHERE ptc.concept_name NOT IN (SELECT DISTINCT documentID FROM '+@eventTable+')'
			;
			EXEC sp_executesql @sql;
			
			SET @msg = 'CASES: PTC added to '+@caseTable;
			RAISERROR(@msg,0,0) WITH NOWAIT;
			SET @msg = '';

	-- =============================================
	-- ADD EVENTS - second level - PTC as anchor
	-- =============================================

		IF OBJECT_ID('tempdb..##eventlog') IS NOT NULL BEGIN
			DROP TABLE ##eventlog;
			SET @msg = 'EVENTS: ##eventlog dropped';
			RAISERROR(@msg,0,0) WITH NOWAIT; 
		END;

		SET @sql = 'DECLARE @eventTable dbo.eventlog
					SELECT *
					INTO ##eventlog
					FROM @eventTable
					WHERE 1=2'
				;
		EXEC sp_executesql @sql;
		
		SET @msg = 'EVENTS: Temp table created';
		RAISERROR(@msg,0,0) WITH NOWAIT;

		--Events PTC
		SET @sql = @commonSql+', c2.concept_name, e.id1,''TravelPermits_eventlog''
					FROM '+@caseTable+' c
					JOIN PrepaidTravelCosts_caselog c2
						ON c2.concept_name = c.sourceID
					JOIN TravelPermits_eventlog e
						ON c2.ID = e.ID
					WHERE e.id1 NOT IN (SELECT sourceID FROM ##eventlog GROUP BY sourceID
										UNION ALL
										SELECT sourceID FROM '+@eventTable+' GROUP BY sourceID)
					';
		EXEC sp_executesql @sql;
		
		SET @msg = 'EVENTS: Travel permits added to '+@eventTable+'. Anchor: PTC.';
		RAISERROR(@msg,0,0) WITH NOWAIT;

		--Events RFP:
		SET @sql = @commonSql+', rfp.concept_name, e.id1,''RequestForPayment_eventlog''
					FROM '+@caseTable+' c
					JOIN PrepaidTravelCosts_caselog c2
						ON c2.concept_name = c.sourceID
					JOIN (SELECT * FROM RequestForPayment_caselog WHERE RfpNumber != ''UNKNOWN'') rfp
						ON rfp.RfpNumber = c2.RfpNumber
					JOIN RequestForPayment_eventlog e
						ON rfp.ID = e.ID
					WHERE e.id1 NOT IN (SELECT sourceID FROM ##eventlog GROUP BY sourceID
										UNION ALL
										SELECT sourceID FROM '+@eventTable+' GROUP BY sourceID)
					';
		EXEC sp_executesql @sql;
		
		SET @msg = 'EVENTS: Request for Request for Payment added to '+@eventTable+'. Anchor: PTC.';
		RAISERROR(@msg,0,0) WITH NOWAIT;

		--persist events
		SET @sql = 'INSERT INTO '+@eventTable+'
					SELECT * FROM ##eventlog'
					;
		EXEC sp_executesql @sql;
		
		SET @msg = 'EVENTS: Persists';
		RAISERROR(@msg,0,0) WITH NOWAIT; 

	END

	IF @add_RFP = 1 BEGIN
	-- =============================================
	-- ADD CASES - first level - RFP as anchor
	-- =============================================

		SET @sql = ' INSERT INTO '+@caseTable+

				   ' SELECT 
					  rfp.concept_name
					, ''Request For Payment''
					, rfp.concept_name
					, NULL
					, NULL
					, NULL
					, NULL
					, NULL
					, NULL
					, NULL
					, NULL
					, NULL
					, NULL
					, NULL
					--new:
					, rfp.Task
					, rfp.Project
					, NULL
					, NULL
					, rfp.Cost_Type AS Cost_Type
					, rfp.OrganizationalEntity
					--added by OL:
					, NULL
					, NULL
					, NULL
					, NULL
					, NULL
					, rfp.RequestedAmount
					, NULL
					, rfp.Activity

				FROM RequestForPayment_caselog rfp
				WHERE rfp.concept_name NOT IN (SELECT DISTINCT documentID FROM '+@eventTable+')'
			;
			EXEC sp_executesql @sql;
			
			SET @msg = 'CASES: RFP added to '+@caseTable;
			RAISERROR(@msg,0,0) WITH NOWAIT;
			SET @msg = '';

	-- =============================================
	-- ADD EVENTS - second level - RFP as anchor
	-- =============================================

		IF OBJECT_ID('tempdb..##eventlog') IS NOT NULL BEGIN
			DROP TABLE ##eventlog;
			SET @msg = 'EVENTS: ##eventlog dropped';
			RAISERROR(@msg,0,0) WITH NOWAIT; 
		END;  
		SET @sql = 'DECLARE @eventTable dbo.eventlog
					SELECT *
					INTO ##eventlog
					FROM @eventTable
					WHERE 1=2'
				;
		EXEC sp_executesql @sql;
		
		SET @msg = 'EVENTS: Temp table created';
		RAISERROR(@msg,0,0) WITH NOWAIT;

	--Events RFP:
		SET @sql = @commonSql+', c2.concept_name, e.id1,''RequestForPayment_eventlog''
					FROM '+@caseTable+' c
					JOIN RequestForPayment_caselog c2
						ON c2.concept_name = c.sourceID
					JOIN RequestForPayment_eventlog e
						ON c2.ID = e.ID
					WHERE e.id1 NOT IN (SELECT sourceID FROM ##eventlog GROUP BY sourceID
										UNION ALL
										SELECT sourceID FROM '+@eventTable+' GROUP BY sourceID)
					';
		EXEC sp_executesql @sql;
		
		SET @msg = 'EVENTS: Request for Request for Payment added to '+@eventTable+'. Anchor: RFP.';
		RAISERROR(@msg,0,0) WITH NOWAIT;

		--persist events
		SET @sql = 'INSERT INTO '+@eventTable+'
					SELECT * FROM ##eventlog'
					;
		EXEC sp_executesql @sql;
		SET @msg = 'EVENTS: Persists';
		
		RAISERROR(@msg,0,0) WITH NOWAIT; 

	END

        --drop global temp table
		IF OBJECT_ID('tempdb..##eventlog') IS NOT NULL BEGIN
			DROP TABLE ##eventlog;
			SET @msg = 'EVENTS: ##eventlog dropped';
			RAISERROR(@msg,0,0) WITH NOWAIT; 
		END;  

        --delete cases without events
		SET @sql = 'DELETE FROM '+@caseTable+'
					WHERE caseID NOT IN (SELECT caseID FROM '+@eventTable+' GROUP BY caseID)
					';
		EXEC sp_executesql @sql;
		
		SET @msg = 'CASES: Cases without events deleted.';
		RAISERROR(@msg,0,0) WITH NOWAIT;
END
