Short desc: This is code created to support data model creation for BPI2020 Challenge.
Author(s): Oskar Leligdowicz, Aleksandra Piasecka, Paul Giessler
Date: 2020-05-02 (first version)
Content:
    We have several files:
        _xesToCsv.py - python script to convert .xes files. It was created for Python 3.74. It is doing following tasks:
            - unpack .gz archive files if needed for all .gz files in same directory
            - check for all .xes files for all .xes files in same directory
            - for every .xes file we treat it as xml file and retreave all cases and events traversing xml file
            - we save results to:
                - csv as default (two files: one for events, one for cases for every .xes file)
                - xlsx as optional (one file with two tabs for every .xes file - used to get first glance into data)
                - database (not implemented, it's faster to use wizard on database to load .csv since we need data only once)
        _createCasesAndEventsMain.sql - sql script which change seperated case and events table into one datamodel with one case and one event table. It was created in SQL Server 2017 db.
        .gz format files - source files provided in the Challenge
        .xes format files -  unpacked files provided in the Challenge
        .csv - output files of python script
        .xlsx - output files of python script
        logs.txt - logs from python script
